import multiprocessing
import time
import logging

from .scoring_server import ScoringServer

log = logging.getLogger('MatchRunner')

class MatchRunner():
  def __init__(self, src_ip="127.0.0.1", src_port=44001, sink_ip="127.0.0.1", sink_port=44002, iq_sink_ip="127.0.0.1", iq_sink_port=33008, config=None):
    self.src_ip = src_ip
    self.src_port = src_port
    self.sink_ip = sink_ip
    self.sink_port = sink_port
    self.iq_sink_ip = iq_sink_ip
    self.iq_sink_port = iq_sink_port
    self.duration = config['duration']
    self.bandwidth = config["bandwidth"]
    self.duration_in_samples = int(self.bandwidth) * self.duration
    log.info("Match config: duration {}, duration_in_samples {}".format(self.duration, self.duration_in_samples))

    self.ss = ScoringServer(self.src_ip, self.src_port, self.sink_ip, self.sink_port, self.iq_sink_ip, self.iq_sink_port, self.bandwidth, self.duration_in_samples, config['traffic_config'])

  def run(self):
    log.info("Starting ScoringServer.")
    self.ss.run()
    log.info("Exited ScoringServer.")

    return 0


def main():
  score = 0
  mr = MatchRunner()
  mr.run()
  log.critical("Done")

  return 0

if __name__ == '__main__':
  main()

