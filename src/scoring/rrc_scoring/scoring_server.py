import secrets
import zmq
import multiprocessing
import threading
import time
import logging
import jwt
import json
import hashlib
import pmt
import socket
import struct
import sys
import os

log = logging.getLogger('ScoringServer')


class ScoringServer():
  def __init__(self, packet_pub_addr="127.0.0.1", packet_pub_port=44001, packet_sub_addr="127.0.0.1", packet_sub_port=44002, iq_sub_addr="127.0.0.1", iq_sub_port=33008, bandwidth=1e6, duration_in_samples=1e6, traffic_config=None, iq_out=True):
    # self.packet_pub_addr = "tcp://" + packet_pub_addr + ":" + str(packet_pub_port)
    self.packet_pub_addr = "tcp://0.0.0.0:" + str(packet_pub_port)
    # self.packet_sub_addr = "tcp://" + packet_sub_addr + ":" + str(packet_sub_port)
    self.packet_sub_addr = "tcp://blue2:" + str(packet_sub_port)
    self.iq_sub_addr = (str(iq_sub_addr), int(iq_sub_port))

    log.debug(self.packet_pub_addr)
    log.debug(self.packet_sub_addr)
    log.debug(self.iq_sub_addr)

    self.packet_pub_proc = threading.Thread(target=self.packet_pub_fn, args=[])
    self.packet_sub_proc = threading.Thread(target=self.packet_sub_fn, args=[])
    self.iq_sub_proc = threading.Thread(target=self.iq_sub_fn, args=[])
    self.packet_pub_proc.daemon = True
    self.packet_sub_proc.daemon = True
    self.iq_sub_proc.daemon = True

    self.bandwidth = bandwidth
    self.traffic_config = traffic_config
    self.bitrate = traffic_config['bitrate']
    self.packet_size = traffic_config['packet_size']
    self.signing_secret = traffic_config['hmac_secret']
    self.packet_period_in_samples = self.bandwidth / self.bitrate * self.packet_size * 8

    self.packet_period = 8.0*self.packet_size / self.bitrate
    self.duration_in_samples = duration_in_samples
    self.running = True

    self.seq_tx = 1
    self.score_lock = threading.Lock()
    self.score = 0
    self.packets_received = 0
    self.packets_sent = 0

    self.sample_offset_lock = threading.Lock()
    self.sample_offset_lock.acquire()
    self.sample_offset_received = 0
    self.sample_offset_lock.release()
    self.sample_offset_sent = 0

    self.iq_out = iq_out

    self.meta = {
      "bandwidth": bandwidth,
      "duration": duration_in_samples/bandwidth,
      "traffic_config": traffic_config,
      "packets_out": [],
      "packets_in": [],
    }

  # convert a byte string to a serialized PMT blob
  def bytes_to_serialized_pmt(self, pld):
    v = pmt.init_u8vector(len(pld), pld)
    d = pmt.cons(pmt.to_pmt(len(pld)), v)
    s = pmt.serialize_str(d)
    return s

  # convert a serialized PMT blob to byte string
  def serialized_pmt_to_bytes(self, pld):
    p = pmt.deserialize_str(pld)
    b = pmt.cdr(p)
    d = pmt.to_python(b)
    s = d.tostring()
    return s

  def packet_pub_fn(self):
    self.packet_pub = zmq.Context().socket(zmq.REP)
    self.packet_pub.bind(self.packet_pub_addr)

    log.info("Starting pub fn")

    while True:
      if not self.running:
        return

      time.sleep(0)

      # wait until the next 
      while (self.sample_offset_sent + self.packet_period_in_samples) < self.sample_offset_received:
        time.sleep(0)
        p = self.build_packet()
        p_pmt = self.bytes_to_serialized_pmt(p)
        self.packet_pub.recv()
        status = self.packet_pub.send(p_pmt)
        self.sample_offset_lock.acquire()
        self.sample_offset_sent += self.packet_period_in_samples
        log.info("sent packet to blue1 at sample offset %d" % self.sample_offset_received)
        log.debug("packet_period_samples: %d" % self.packet_period_in_samples)
        log.debug("sample_offset_sent: %d" % self.sample_offset_sent)
        log.debug("sample_offset_received: %d" % self.sample_offset_received)
        self.sample_offset_lock.release()

  def packet_sub_fn(self):
    self.packet_sub = zmq.Context().socket(zmq.REQ)
    self.packet_sub.connect(self.packet_sub_addr)

    # zero-out score file from prior run
    with open("/scripts/scoring/score.txt", "wb") as f:
      f.write(b"%d" % 0)

    while True:
      time.sleep(0)
      if not self.running:
        return
      
      if True:

        # send the requisite request for the ZMQ reqest -> reply patterm
        self.packet_sub.send(struct.pack("I", 1))

        # read the next packet from blue2
        r = self.packet_sub.recv()
        p = None
        r = self.serialized_pmt_to_bytes(r)

        # Try to decode the JWT
        try:
          #p = jwt.decode(r, "bad key", algorithm="HS256", verify=True)
          log.info(r)
          p = jwt.decode(r, self.signing_secret, algorithm="HS256", verify=True)
        except jwt.exceptions.InvalidSignatureError:
          log.info("Failed signature check!")

        # if the decode worked, process its sequence number
        if p is not None:
          log.info("Received seq: {}".format(p["seq"]))

          # if the sequence number is the current score + 1, it's a contiguous monotonic packet so score it
          self.score_lock.acquire()
          if p["seq"] == (self.score + 1):
            log.info("New score: {}".format(p["seq"]))

            # TODO argparse config
            with open("/scripts/scoring/score.txt", "wb") as f:
              f.write(b"%d" % p["seq"])

            self.score = p["seq"]
          # else we dropped or got a duplicate, so no more packets will score
          else:
            log.warn("MISS: Expecting {}, got {}".format(self.score+1, p["seq"]))

          self.meta["packets_in"].append({
            "time":  self.sample_offset_received / self.bandwidth,
            "seq": p["seq"],
            "score": self.score,
          })

          self.score_lock.release()

      else:
        time.sleep(0.01)

  def iq_sub_fn(self):
    log.info("Starting sub fn")
    self.iq_sub = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

    connected = False
    while connected is False:
      try:
        self.iq_sub.connect((self.iq_sub_addr))
      except Exception as e:
        log.info("Not connected, waiting 1s...")
        time.sleep(1)
        continue

      connected = True
      log.info("Connected, proceeding.")

    self.iq_sub.sendall(struct.pack("N", 0))

    if self.iq_out:
      iq_out_file = open("/scripts/scoring/match.iq", "wb")

    while True:
      time.sleep(0)

      # read the header
      remaining = 24
      data = b""
      while remaining > 0:
        r = self.iq_sub.recv(remaining)
        data += r
        remaining -= len(r)
        time.sleep(0)

      # unpack the header
      offset, length, sob, eob = struct.unpack("NNII", data)

      # read (and discard) the remaining IQ data
      remaining = length * 8
      remaining = 1000 * 8
      # log.debug("offset, length, sob, eob, remaining = %lu %lu %u %u %lu" % (offset, length, sob, eob, remaining))
      while remaining > 0:
        r = self.iq_sub.recv(remaining)

        # write the output IQ
        written = 1000*8 - remaining
        to_write = min(length*8-written, len(r))
        if self.iq_out:
          iq_out_file.write(r[:to_write])
          iq_out_file.flush()

        remaining -= len(r)
        time.sleep(0)

      # update sample_offset_received, which represents the match clock in samples
      self.sample_offset_lock.acquire()
      self.sample_offset_received = offset
      self.sample_offset_lock.release()

      # send the next sample offset to the muxer
      self.iq_sub.sendall(struct.pack("N", offset+length))

    if self.iq_out:
      iq_out_file.close()

  def build_packet(self):
    ''' packet format:
    jwt
    '''

    #r = str(secrets.token_bytes(nbytes=self.packet_size))
    r = str(secrets.token_bytes(nbytes=self.packet_size).hex())
    p = {"seq": self.seq_tx, \
         "payload": r}
    packet = jwt.encode(p, self.signing_secret, algorithm="HS256")
    # log.info("Sending seq: {}, length: {}".format(self.seq_tx, len(packet)))
    self.seq_tx += 1

    return packet

  def run(self):
    self.packet_sub_proc.start()
    self.packet_pub_proc.start()
    self.iq_sub_proc.start()

    log.info("Running...")
    while True:
      time.sleep(0.01)
      if self.sample_offset_received >= self.duration_in_samples:
        log.info("End of match of duration_in_samples {} at sample_offset_received {}".format(self.duration_in_samples, self.sample_offset_received))
        break

    self.stop()

    with open("/scripts/scoring/match-meta.json", "wb") as f:
      f.write(json.dumps(self.meta).encode())

    return 0

  def stop(self):
    log.info("Stopping...")
    self.running = False
    time.sleep(1.0)
    self.packet_pub_proc.join()
    self.packet_sub_proc.join()
    log.info("Stopped.")
    return 0


def main():
  ss = ScoringServer()
  ss.run()

  return 0

if __name__ == '__main__':
  main()

