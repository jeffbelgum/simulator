/* -*- c++ -*- */
/*
 * Copyright 2020 gr-rircsim author.
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

#ifndef INCLUDED_RIRCSIM_MUXER_IQ_SINK_IMPL_H
#define INCLUDED_RIRCSIM_MUXER_IQ_SINK_IMPL_H

#include <rircsim/muxer_iq_sink.h>
#include <boost/asio.hpp>
#include <boost/bind.hpp>
#include <boost/thread.hpp>

#include "muxer_config.h"

using boost::asio::ip::tcp;

namespace gr {
  namespace rircsim {

    enum node_sink_state_t
    {
      NODE_SINK_START,
      NODE_SINK_CONNECTED,
    };

    class muxer_iq_sink_impl : public muxer_iq_sink
    {
     private:

      void write(std::complex<float> * samples, size_t length);

      std::string host;
      uint16_t port;
      node_sink_state_t state;

      size_t sample_offset;
      muxer_iq_chunk_t iq_chunk;

      boost::asio::io_service io_service;
      boost::shared_ptr<tcp::socket> socket;
      boost::thread m_thread;

      boost::thread m_write_thread;
      std::vector<std::complex<float> > m_write_buffer;
      int m_write_length;
      uint64_t m_end_of_burst;
      std::mutex m_write_mutex;
      volatile bool m_samples_to_write;
      // volatile bool m_end_of_burst;

      void client_loop();
      void socket_read_handler(const boost::system::error_code& ec, std::size_t bytes_transferred);
      void write_loop();
      volatile bool m_cancel;

      FILE * m_file;

      std::string m_name;

     public:
      muxer_iq_sink_impl(std::string host, uint16_t port, std::string name);
      ~muxer_iq_sink_impl();

      bool stop();

      // Where all the action really happens
      int work(
              int noutput_items,
              gr_vector_const_void_star &input_items,
              gr_vector_void_star &output_items
      );
    };

  } // namespace rircsim
} // namespace gr

#endif /* INCLUDED_RIRCSIM_MUXER_IQ_SINK_IMPL_H */

