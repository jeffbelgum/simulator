/* -*- c++ -*- */
/*
 * Copyright 2020 RIRC.
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <gnuradio/io_signature.h>
#include <volk/volk.h>
#include "muxer_impl.h"

namespace gr {
  namespace rircsim {

    muxer::sptr
    muxer::make()
    {
      return gnuradio::get_initial_sptr
        (new muxer_impl());
    }


    /*
     * The private constructor
     */
    muxer_impl::muxer_impl()
      : gr::block("muxer",
              gr::io_signature::make(3, 3, sizeof(gr_complex)),
              gr::io_signature::make(1, 1, sizeof(gr_complex)))
    {

    }

    /*
     * Our virtual destructor.
     */
    muxer_impl::~muxer_impl()
    {
    }

    void
    muxer_impl::forecast (int noutput_items, gr_vector_int &ninput_items_required)
    {
      ninput_items_required[0] = 0;
      ninput_items_required[1] = 0;
      ninput_items_required[2] = 0;
    }

    int 
    muxer_impl::general_work(int noutput_items,
           gr_vector_int &ninput_items,
           gr_vector_const_void_star &input_items,
           gr_vector_void_star &output_items)
    {
      gr_complex *out = (gr_complex *) output_items[0];
      
      const gr_complex *blue1 = (gr_complex *) input_items[0];
      const gr_complex *blue2 = (gr_complex *) input_items[1];
      const gr_complex *red1 = (gr_complex *) input_items[2];

      int count_blue1 = ninput_items[0];
      int count_blue2 = ninput_items[1];
      int count_red1 = ninput_items[2];

      count_blue1 = std::min(count_blue1, noutput_items);
      count_blue2 = std::min(count_blue2, noutput_items);
      count_red1 = std::min(count_red1, noutput_items);

      memset(out, 0, noutput_items*sizeof(gr_complex));

      // sum the IQ from each source
      volk_32fc_x2_add_32fc(out, blue1, out, count_blue1);
      volk_32fc_x2_add_32fc(out, blue2, out, count_blue2);
      volk_32fc_x2_add_32fc(out, red1, out, count_red1);  
      
      consume(0, count_blue1);
      consume(1, count_blue2);
      consume(2, count_red1);

      return noutput_items;
    }

  } /* namespace rircsim */
} /* namespace gr */

