/* -*- c++ -*- */
/*
 * Copyright 2020 RIRC.
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

#ifndef INCLUDED_RIRCSIM_LIQUID_FLEX_RX_IMPL_H
#define INCLUDED_RIRCSIM_LIQUID_FLEX_RX_IMPL_H

#include <rircsim/liquid_flex_rx.h>
#include <liquid/liquid.h>

namespace gr {
  namespace rircsim {

    class liquid_flex_rx_impl : public liquid_flex_rx
    {
     private:
      flexframesync m_framesync;
      flexframegenprops_s m_fgprops;
      // void in_iq_handler(pmt::pmt_t msg);
      static int frame_callback(unsigned char *  _header,
                    int              _header_valid,
                    unsigned char *  _payload,
                    unsigned int     _payload_len,
                    int              _payload_valid,
                    framesyncstats_s _stats,
                    void *           _userdata);

     public:
      liquid_flex_rx_impl();
      ~liquid_flex_rx_impl();

      int general_work(int noutput_items,
           gr_vector_int &ninput_items,
           gr_vector_const_void_star &input_items,
           gr_vector_void_star &output_items);      
    };

  } // namespace rircsim
} // namespace gr

#endif /* INCLUDED_RIRCSIM_LIQUID_FLEX_RX_IMPL_H */

