#pragma once

#include <complex>

namespace gr {
  namespace rircsim {

    // #define MUXER_DEBUG

    #define BLUE1_IN_PORT 33001
    #define BLUE1_OUT_PORT 33002

    #define BLUE2_IN_PORT 33003
    #define BLUE2_OUT_PORT 33005
    
    #define RED1_IN_PORT 33004
    #define RED1_OUT_PORT 33007
    
    #define CHANNEL_IN_PORT 33006

    #define MARSHAL_OUT_PORT 33008
    
    #define HOST_OUT_PORT 44006

    // maximum number of complex samples per muxer IQ chunk
    const size_t MUXER_IQ_CHUNK_MAX_LENGTH = 1000;

    struct muxer_iq_chunk_t
    {
      size_t offset;
      size_t length;
      uint32_t start_of_burst;
      uint32_t end_of_burst;
      std::complex<float> data[MUXER_IQ_CHUNK_MAX_LENGTH];
    };

  }
}

