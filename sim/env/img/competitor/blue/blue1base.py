#!/usr/bin/env python3
# -*- coding: utf-8 -*-

#
# SPDX-License-Identifier: GPL-3.0
#
# GNU Radio Python Flow Graph
# Title: Blue 1 Base Flowgraph
# GNU Radio version: 3.8.1.0

from gnuradio import blocks
from gnuradio import gr
from gnuradio.filter import firdes
import sys
import signal
from argparse import ArgumentParser
from gnuradio.eng_arg import eng_float, intx
from gnuradio import eng_notation
from gnuradio import zeromq
import rircsim

class blue1base(gr.top_block):

    def __init__(self):
        gr.top_block.__init__(self, "Blue 1 Base Flowgraph")

        ##################################################
        # Variables
        ##################################################
        self.samp_rate = samp_rate = 1e6
        self.muxer_hostname = muxer_hostname = "muxer"

        ##################################################
        # Blocks
        ##################################################
        self.zeromq_req_msg_source_0 = zeromq.req_msg_source('tcp://marshal:44001', 1000)
        self.rircsim_muxer_iq_source_0 = rircsim.muxer_iq_source(muxer_hostname, 33002)
        self.rircsim_muxer_iq_sink_0 = rircsim.muxer_iq_sink(muxer_hostname, 33001, 'blue1')
        self.rircsim_liquid_flex_tx_0 = rircsim.liquid_flex_tx(1, 4, 1)
        self.blocks_null_sink_0 = blocks.null_sink(gr.sizeof_gr_complex*1)



        ##################################################
        # Connections
        ##################################################
        self.msg_connect((self.zeromq_req_msg_source_0, 'out'), (self.rircsim_liquid_flex_tx_0, 'in_pdu'))
        self.connect((self.rircsim_liquid_flex_tx_0, 0), (self.rircsim_muxer_iq_sink_0, 0))
        self.connect((self.rircsim_muxer_iq_source_0, 0), (self.blocks_null_sink_0, 0))

    def get_samp_rate(self):
        return self.samp_rate

    def set_samp_rate(self, samp_rate):
        self.samp_rate = samp_rate

    def get_muxer_hostname(self):
        return self.muxer_hostname

    def set_muxer_hostname(self, muxer_hostname):
        self.muxer_hostname = muxer_hostname



def main(top_block_cls=blue1base, options=None):
    tb = top_block_cls()

    def sig_handler(sig=None, frame=None):
        tb.stop()
        tb.wait()
        sys.exit(0)

    signal.signal(signal.SIGINT, sig_handler)
    signal.signal(signal.SIGTERM, sig_handler)

    tb.start()
    tb.wait()


if __name__ == '__main__':
    main()
